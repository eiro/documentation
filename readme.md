# Formations / Documentations

# Sympa

## Audience

Vous êtes un personnel non technique et souhaitez organiser la diffusion
d'information, le partage de documents, ou la création de canaux de discutions

* au sein de votre structure
* entre votre structure et des populations exterieures

Les listes de diffusion sont probablement le bon outils.

## Programme

* définitions de base
  * domaine
  * adresse e-mail
  * annuaire
  * liste de contacts
  * liste de diffusion
  * publipostage
* que c'est-il passé lorsque j'ai cliqué sur "envoyer" ?
* du bon usage des  groupes de contacts, comptes non-nominatifs et
  listes de diffusion
* spam, anti-spam, fishing, ...
  explications et bases de self-defense numérique
* organisation du travail dans sympa (communautés, groupes de travail,
  listmasters, propriétaires, modérateurs, abonnés, ...)
* création et paramètrage de listes
  * les modèles de listes
  * les options de listes
  * les roles, droits, propriétés, délégations
  * les familles de listes
* automatisation
    * sources externes
    * croisements de listes
    * scenarii et réponses automatiques


# Perl bases

## Audience

Déjà coutumier d'un langage interprété (python, javascript, ...)
Vous êtes à l'aise avec les notions de base de programmation
(structures de controle, fonctions, espaces de noms, scopes, ...)

* les bases de la syntax

# Vim bases

## Audience

Pourtant difficile d'accès, vim est depuis longtemps un éditeur très populaire
dans le monde unix. La pérénité de ce succès réside dans la puissance et
la flexibilité de cet outils.

Si vous éditez des fichiers sur un serveur distant, vim est un must de votre
boite à outils. Il est aussi possible d'utiliser vim dans un environement graphique
pour effectuer rapidement des modifications importantes de fichiers/

## Programme

* l'environement de vim
  * les fenêtres, buffers, la commande
* l'édition modale:
  * description des principaux modes (édition, normal, commande, visuel)
  * navigation entre les modes
* le mode normal: tirer partie de la puissnce de vim pour
  * naviguer dans un texte
  * editer du texte
* l'aide en ligne: comment trouver facilement la bonne documentation
* personalisations simples
  * paramètres
  * mappings

# Linux bases

## Programme

* composition d'un unix (kernel, init, userland, swap, ...)
* l'organisation des fichiers sous unix
* GNOME, le bureau graphique
* les bases de shell
* les distributions linux
* les bases de l'administration
  * installer des logiciels
  * automatiser des taches

# Shell scripting avec zsh et uze

## Programme

* la syntaxe de base
* vos premieres commandes
* les structures de contrôle
* les structures de données
* les redirections, les fifos et les pipes
* introductions aux filtres standard
* construire des bibliothèques

# SSH

## Programme

* pourquoi et comment chiffrer ?
* fonctionnement de base de ssh
* découverte de scp
* gestion des clefs
* utilisation d'agents
* rebonds et transmissions de ports
* sécurité et restrictions
* scripter avec ssh
